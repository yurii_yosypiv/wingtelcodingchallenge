"""Helper functions for cross-part usage"""

from src.constants import DEFAULT_TRAFFIC_PRECISION


def convert_traffic(from_size, to_size, traffic,
                    precision=DEFAULT_TRAFFIC_PRECISION):
    """Helper method converting traffic from one units to other

    Args:
        from_size (float): size of current units
        to_size (float): size of target units
        traffic (float): traffic to convert
        precision (int): number of digits after decimal point
    Returns:
        float: converted traffic
    """
    result = round(
        traffic * (from_size / to_size),
        precision
    )

    return result
