DATA_BLOCK_CODE_ID = 1


# traffic related constants
MB_SIZE = 2 ** 20
GB_SIZE = 2 ** 30

DEFAULT_TRAFFIC_PRECISION = 3
