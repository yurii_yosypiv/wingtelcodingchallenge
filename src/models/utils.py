"""Utilities for models to inherit or use"""
from sqlalchemy.exc import SQLAlchemyError
from flask import abort
from http import HTTPStatus


def get_object_or_404(model, mid):
    """Get an object by id or return a 404 not found response

    Args:
        model (object): object's model class
        mid (int): object's id

    Returns:
        object: returned from query

    Raises:
        404: if one object is returned from query

    """
    result = model.query.get(mid)
    if not result:
        abort(404, f"Object {model.__name__} with id={mid} does not exist")
    return result


def query_result_to_dict(record):
    """Convert query result (result object containing one record) to dict
    e.g. when we need to obtain complex results without ability to use models

    Args:
        record (result object): result object containing one record

    Returns:
        list: list of dicts containing row data

    """
    result = {}
    for column in record._fields:
        result[column] = str(getattr(record, column))
    return result
