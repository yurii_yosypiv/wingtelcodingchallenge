"""Subscription schemas to assist with sub serialization"""
from marshmallow import fields, Schema, validate

from src.schemas.service_codes import PlanSchema
from src.constants import MB_SIZE, GB_SIZE
from src.utils import convert_traffic


class SubscriptionSchema(Schema):
    """Schema class to handle serialization of subscription data"""
    id = fields.Integer()
    phone_number = fields.String()

    status = fields.String()
    status_effective_date = fields.DateTime()

    plan_id = fields.String()
    plan = fields.Nested(PlanSchema, dump_only=True)
    service_codes = fields.Method("get_service_codes")

    @staticmethod
    def get_service_codes(obj):
        """field get method to return service code names"""
        return [code.name for code in obj.service_codes]


class SubscriptionUsageSchema(Schema):
    """Schema class to handle serialization of subscription's data usage"""
    subscription_id = fields.Integer()
    total_usage = fields.Method("convert_mb_to_gb")

    @staticmethod
    def convert_mb_to_gb(obj):
        """Method converting data usage from MB to GB"""
        return convert_traffic(MB_SIZE, GB_SIZE, obj.total_usage)
