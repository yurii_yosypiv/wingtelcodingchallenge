"""Module to house main celery tasks"""
from src.tasks import block_subscriptions
from src.tasks import check_blocked_subscriptions
