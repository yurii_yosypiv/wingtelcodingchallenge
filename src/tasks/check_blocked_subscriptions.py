"""Task blocking subscriptions which are over their allotted data"""
from sqlalchemy import func, and_

from src.constants import DATA_BLOCK_CODE_ID

from src.models.base import db
from src.models.utils import get_object_or_404, query_result_to_dict
from src.models.cycles import BillingCycle
from src.models.service_codes import subscriptions_service_codes
from src.models.versioning import SubscriptionServiceChange
from src.models.usages import DataUsage
from src.models.subscriptions import Subscription, SubscriptionStatus


# TODO: need to configure celery scheduling
# @celery.task()
def check_blocked_subscriptions(bc_id=None):
    if bc_id:
        cycle = get_object_or_404(BillingCycle, bc_id)
    else:
        cycle = BillingCycle.get_current_cycle()

    if cycle:
        usage = traffic_overuse(cycle)
        result = [query_result_to_dict(record) for record in usage]
        print(result)
        return result
        # TODO: add some use case / alert to visualize data


def traffic_overuse(cycle):
    """Gets usage for blocked subscriptions if any
    was generated after block applied

    Returns:
        list: list of objects returned from query result
    """

    # 1) Calculated traffic overuse for blocked subscriptions
    overuse_subq = db.session.query(
        DataUsage.subscription_id,
        SubscriptionServiceChange.service_code_id,
        SubscriptionServiceChange.change_date,
        func.sum(DataUsage.mb_used).label('traffic_overuse'),
    ).join(
        SubscriptionServiceChange,
        SubscriptionServiceChange.subscription_id == DataUsage.subscription_id,
    ).filter(
        SubscriptionServiceChange.event_type == 'added',
        DataUsage.from_date >= cycle.start_date,
        DataUsage.from_date >= SubscriptionServiceChange.change_date,
        DataUsage.from_date < cycle.end_date
    ).group_by(
        DataUsage.subscription_id
    ).subquery()

    # 2) Join effective subscriptions with status blocked
    # with subquery calculating traffic overuse
    result = db.session.query(
        Subscription.id.label('subscription_id'),
        overuse_subq.c.change_date.label('date_of_block'),
        overuse_subq.c.traffic_overuse,
    ).join(
        subscriptions_service_codes,
        subscriptions_service_codes.c.subscription_id == Subscription.id
    ).join(
        overuse_subq,
        and_(overuse_subq.c.subscription_id == Subscription.id,
             overuse_subq.c.service_code_id == subscriptions_service_codes.c.service_code_id)
    ).filter(
        Subscription.status.in_([SubscriptionStatus.active, SubscriptionStatus.suspended]),
        subscriptions_service_codes.c.service_code_id == DATA_BLOCK_CODE_ID,
    )

    return result
