"""Task blocking subscriptions which are over their allotted data"""
from sqlalchemy import func, and_

from src.constants import DATA_BLOCK_CODE_ID

from src.models.base import db
from src.models.utils import get_object_or_404
from src.models.cycles import BillingCycle
from src.models.service_codes import Plan, subscriptions_service_codes
from src.models.usages import DataUsage
from src.models.subscriptions import Subscription, SubscriptionStatus


# TODO: need to configure celery scheduling
# @celery.task()
def block_subscriptions(bc_id=None):
    # list to insert into table subscriptions_service_codes
    to_insert = []

    if bc_id:
        cycle = get_object_or_404(BillingCycle, bc_id)
    else:
        cycle = BillingCycle.get_current_cycle()

    if cycle:
        usage = get_subscriptions_usage(cycle)
        for record in usage:
            if record.total_usage >= record.mb_available and not record.is_unlimited:
                to_insert.append({
                    "subscription_id": record.subscription_id,
                    "service_code_id": DATA_BLOCK_CODE_ID
                })
        if to_insert:
            insert_stmt = subscriptions_service_codes.insert().values(to_insert)
            db.session.execute(insert_stmt)
            db.session.commit()
    return to_insert


def get_subscriptions_usage(cycle):
    """Gets total data usage for all subscriptions
    which are not blocked
    for given billing cycle

    Returns:
        list: list of objects returned from query result
    """

    # 1) subquery to get total usages for all subscriptions which are not blocked
    # using subquery gives us better performance on JOIN with Plan
    # because we are joining already grouped data.
    # 2) left outer join - is a hack to get only subscription with service_code != null
    usage_subq = db.session.query(
        DataUsage.subscription_id,
        func.sum(DataUsage.mb_used).label('total_usage'),
    ).outerjoin(
        subscriptions_service_codes,
        and_(DataUsage.subscription_id == subscriptions_service_codes.c.subscription_id,
             subscriptions_service_codes.c.service_code_id == DATA_BLOCK_CODE_ID)
    ).filter(
        DataUsage.from_date >= cycle.start_date,
        DataUsage.from_date < cycle.end_date,
        subscriptions_service_codes.c.subscription_id.is_(None)
    ).group_by(
        DataUsage.subscription_id
    ).subquery()

    # 3) join with plans to get available traffic
    result = db.session.query(
        usage_subq.c.subscription_id,
        usage_subq.c.total_usage,
        Plan.mb_available,
        Plan.is_unlimited,
    ).join(
        Subscription,
        usage_subq.c.subscription_id == Subscription.id
    ).join(
        Plan,
        Subscription.plan_id == Plan.id
    ).filter(
        Subscription.status.in_([SubscriptionStatus.active,
                                SubscriptionStatus.suspended,
                                SubscriptionStatus.expired]),
    ).all()

    return result
