"""Usage resource for handling any usage requests"""
from flask import jsonify
from flask_restful import Resource

from src.models.subscriptions import Subscription
from src.models.cycles import BillingCycle
from src.models.utils import get_object_or_404
from src.schemas.subscriptions import SubscriptionUsageSchema


class SubscriptionUsageAPI(Resource):
    """Resource/routes for subscription's usage endpoints"""

    @staticmethod
    def set_is_over_flag(usage, subscription):
        """Helper method updating usage with flag is_over"""

        # TODO: How to track plan for given cycle?
        is_over = (not subscription.plan.is_unlimited and
                   usage.total_usage >= subscription.plan.mb_available)
        usage['is_over'] = is_over

    def get(self, sid):
        """External facing subscription's usage endpoint GET

        Gets subscriptions's data usage for current billing cycle
         if subscription with given id exists

        Args:
            sid (int): id of subscription object

        Returns:
            json: serialized subscription object
        """

        subscription = get_object_or_404(Subscription, sid)
        cycle = BillingCycle.get_current_cycle() or BillingCycle.get_last_cycle()
        usage = subscription.get_usage_for_cycle(cycle)
        result = SubscriptionUsageSchema().dump(usage).data
        if result:
            self.set_is_over_flag(result, subscription)
        return jsonify(result)

